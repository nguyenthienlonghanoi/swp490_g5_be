package com.wfms.Dto;

import lombok.Data;

@Data
public class RoleToUserForm {
    private String username;
    private Long roleId;
    private String jobTitle;
}
