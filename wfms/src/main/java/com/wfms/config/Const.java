package com.wfms.config;

public class Const {
    public static final Long TASK_TYPE_STORY =1L;
    public static class responseError{
        public static final String content_null = "Content must not be null";
        public static final String taskId_null = "TaskId must not be null";
        public static final String sprintId_null = "SprintId must not be null";
        public static final String userId_null = "UserId must not be null";
        public static final String workflowId_null = "WorkflowId must not be null";
        public static final String userName_null = "UserName must not be null ";
        public static final String address_null = "Address must not be null ";
        public static final String role_null = "Role must not be null ";
        public static final String fullName_null = "FullName must not be null ";
        public static final String gender_null = "Gender must not be null ";
        public static final String jobtitle_null = "Jobtitle must not be null ";
        public static final String phoneNumber_null = "Phone number must not be null ";
        public static final String email_null = "Email must not be null ";
        public static final String deadline_null = "Deadline must not be null ";
        public static final String UserNameEmailExsist = "UserName or Emial is exsist ";
        public static final String birthDay_null = "Date of birth must not be null ";
        public static final String projectId_null = "ProjectId must not be null";
        public static final String priorityId_null = "PriorityId must not be null";
        public static final String lead_null = "Lead must not be null";
        public static final String project_notFound = "Not found project with ID ";
        public static final String user_notFound = "Not found user with ID ";
        public static final String priority_notFound = "Not found priority with ID ";
        public static final String dailyReportId_null = "DailyReportId must not be null";
        public static final String dailyReport_notFound = "Not found daily report with ID ";
        public static final String task_notFound = "Not found task with ID ";
        public static final String sprint_notFound = "Not found sprint with ID ";
        public static final String workflow_notFound = "Not found workflow with ID ";
    }
}
