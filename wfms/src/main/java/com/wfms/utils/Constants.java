package com.wfms.utils;

public class Constants {
    public static Long EXTREME = 1L;
    public static Long HIGH = 2L;
    public static Long MODERATE = 3L;
    public static Long LOW = 4L;

    public static double PERIOD_1 = 0.15;
    public static double PERIOD_2 = 0.3;
    public static double PERIOD_3 = 0.4;

    public static String FILE_USER_NAME = "template_users.xlsx";
    public static String TEMPLATE_USER = "template_create_user.xlsx";
    public static final String PATH_TEMPLATE = "classpath:templates/";

    public static Integer ONLINE_ROOM = 1;
    public static Integer OFFLINE_ROOM = 2;

    public static Integer ACTIVE = 3;
    public static Integer NORMAL_ACTIVE = 1;
    public static Integer NOT_START = 1;
    public static Integer CLOSE = 2;
    public static Integer INACTIVE = 0;



}
