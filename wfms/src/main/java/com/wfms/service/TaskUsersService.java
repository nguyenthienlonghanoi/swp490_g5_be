package com.wfms.service;

import com.wfms.entity.TaskUsers;

public interface TaskUsersService {
    TaskUsers createTaskUser(TaskUsers taskUsers);
}
