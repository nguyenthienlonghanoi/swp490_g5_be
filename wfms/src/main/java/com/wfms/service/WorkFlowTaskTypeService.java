package com.wfms.service;

import com.wfms.entity.WorkFlowTaskType;

public interface WorkFlowTaskTypeService {

    WorkFlowTaskType createWorkFlowTaskType(WorkFlowTaskType workFlowTaskType);
    WorkFlowTaskType updateWorkFlowTaskType(WorkFlowTaskType workFlowTaskType);
}
